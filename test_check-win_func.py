import pytest
from portable import GameFunc


@pytest.mark.parametrize("deck, res", [
    (
            ["X", "X", "X",
             4, 5, 6,
             7, 8, 9],
            "X"),
    (
            [1, 2, 3,
             "X", "X", "X",
             7, 8, 9],
            "X"),
    (
            [1, 2, 3,
             4, 5, 6,
             "X", "X", "X"],
            "X")
])
def test_horis(deck, res):
    g = GameFunc(deck)

    assert g.winch() == res


@pytest.mark.parametrize("deck, res", [
    (
            ["X", 2, 3,
             "X", 5, 6,
             "X", 8, 9],
            "X"),
    (
            [1, "X", 3,
             4, "X", 6,
             7, "X", 9],
            "X"),
    (
            [1, 2, "X",
             4, 5, "X",
             7, 8, "X"],
            "X")
])
def test_verti(deck, res):
    g = GameFunc(deck)

    assert g.winch() == res


@pytest.mark.parametrize("deck, res", [
    (
            ["X", 2, 3,
             4, "X", 6,
             7, 8, "X"],
            "X"),
    (
            [1, 2, "X",
             4, "X", 6,
             "X", 8, 9],
            "X")
])
def test_diago(deck, res):
    g = GameFunc(deck)

    assert g.winch() == res
