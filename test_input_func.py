import pytest
import sys
from portable import GameFunc


@pytest.mark.parametrize("inp, res", [(1, 1),
                                      (2, 2),
                                      (3, 3),
                                      (4, 4),
                                      (5, 5),
                                      (6, 6),
                                      (7, 7),
                                      (8, 8),
                                      (9, 9)])
def test_valid_integers(inp, res, capsys):
    deck = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    g = GameFunc(deck)
    assert g.turn(1, inp) == res


@pytest.mark.parametrize("inp", [0, (-1), 10])
def test_invalid_integers(inp, capsys):
    deck = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    g = GameFunc(deck)
    g.turn(1, inp)
    captured = capsys.readouterr()
    assert captured.out == "Русским языком написано: от 1 до 9\n"


@pytest.mark.parametrize("inp", ["ы", "F", "\n"])
def test_invalid_chars(inp, capsys):
    deck = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    g = GameFunc(deck)
    g.turn(1, inp)
    captured = capsys.readouterr()
    assert captured.out == "Че?\n"
