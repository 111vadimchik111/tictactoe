from custom_error import *


class GameFunc:
    def __init__(self, deck):
        self.deck = deck

    def decorator(func) -> list:
        def wrapper(self, i):
            func(self, 0)
            print('-+-+-')
            func(self, 3)
            print('-+-+-')
            func(self, 6)

        return wrapper

    @decorator
    def printdeck(self, i):
        print(f"{self.deck[i]}|{self.deck[i + 1]}|{self.deck[i + 2]}")

    def winch(self) -> list:
        win_con = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
        for i in win_con:
            if self.deck[i[0]] == self.deck[i[1]] == self.deck[i[2]]:
                return self.deck[i[0]]

    def turn(self, n, i) -> int or str:
        n += 1
        try:
            answer = int(i)
        except ValueError:
            answer = "0"
        if answer != "0":
            if 0 < answer < 10:
                if self.deck[answer - 1] == answer:
                    return answer
                else:
                    print('Занято!')
            else:
                print('Русским языком написано: от 1 до 9')
        else:
            print('Че?')
            return None
        return None

    def endgame(self, n, xo, cord):
        if n == 10:
            print("игрок", xo, "проиграл по причине: закончились попытки")
            if xo == 'X':
                xo = 'O'
            else:
                xo = 'X'
            return xo, "draw"
        self.deck[cord] = xo
        if self.winch():
            return xo, "win"
        if xo != 'X':
            xo = 'X'
        else:
            xo = 'O'
        return xo, None


def main():
    deck = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    g = GameFunc(deck)
    xo = "X"
    i = 0
    for i in range(9):
        g.printdeck(i)
        print("Ходит", xo)
        print(f"{xo},Введите число от 1 до 9")
        n = 0
        while n < 10:
            try:
                i = input()
            except EOFError:
                print('Завершение работы программы')
                exit()
            output = g.turn(n, i)
            if output is None:
                n += 1
            else:
                if 0 < output < 10:
                    if n > 1:
                        print("Наконец то")
                    cord = output - 1
                    break
        xo, end = g.endgame(n, xo, cord)
        if end == "win":
            g.printdeck(0)
            print('Победил', xo)
            break
        elif end == "draw":
            g.printdeck(0)
            print('Ничья')
            break


if __name__ == "__main__":
    main()
